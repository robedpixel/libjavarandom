#ifndef CDRNG_H
#define CDRNG_H

#ifdef _MSC_VER
#include <intrin.h>
#elif defined(__GNUG__)
#include <cpuid.h>
#endif

#include "CPURand.h"
#include "RDRANDRand.h"
#include "TPMRand.h"
#include "cDrng_global.h"
#include <vector>

class CDRNG_EXPORT CDrng {
public:
  // checks for availability of RNG and picks best one
  CDrng();
  ~CDrng();
  void InitialiseEngine(uint8_t level);
  static int8_t GetRNGQuality(CDrng *obj);
  static int16_t GetRandShort(CDrng *obj);
  static int32_t GetRandInt(CDrng *obj);
  static int64_t GetRandLong(CDrng *obj);
  static const uint8_t level_rdrand = 2;
  static const uint8_t level_tpm = 1;
  static const uint8_t level_cpu = 0;

private:
  char rd_level;
  std::vector<uint16_t> shrt_buf;
  std::vector<uint32_t> int_buf;
  std::vector<unsigned long long> long_buf;
  RandEngine *rng_pointer = nullptr;
};

#endif // CDRNG_H
