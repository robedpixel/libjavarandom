#include "CPURand.h"

CPURand::CPURand() {
  cpu_engine =
      new std::independent_bits_engine<std::default_random_engine, 16,
                                       unsigned short>(std::random_device{}());
  buffer.resize(2);
  long_buffer.resize(4);
}
CPURand::~CPURand() { delete cpu_engine; }

unsigned int CPURand::GetRandUShorts(std::vector<uint16_t> *in) {
  std::generate(in->begin(), in->end(), std::ref(*cpu_engine));
  return 0;
}
unsigned int CPURand::GetRandUInts(std::vector<uint32_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    std::generate(buffer.begin(), buffer.end(), std::ref(*cpu_engine));
    in->operator[](i) =
        static_cast<uint32_t>(static_cast<uint32_t>(buffer[0]) << 16 |
                              static_cast<uint32_t>(buffer[1]));
  }
  return 0;
}
unsigned int CPURand::GetRandULongs(std::vector<unsigned long long> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    std::generate(long_buffer.begin(), long_buffer.end(), std::ref(*cpu_engine));
    in->operator[](i) =
        static_cast<unsigned long long>(static_cast<unsigned long long>(buffer[0]) << 48 |
                              static_cast<unsigned long long>(buffer[1]) << 32 |
                              static_cast<unsigned long long>(buffer[2]) << 16 |
                              static_cast<unsigned long long>(buffer[3]));
  }
  return 0;
}
