#include "cdrng.h"

CDrng::CDrng() {
  shrt_buf.resize(1);
  int_buf.resize(1);
  long_buf.resize(1);
  // check for rdrand instruction using eax

  rd_level = 0;
#ifdef _MSC_VER
  int cpu_info[4];
  __cpuidex(cpu_info, 1, 0);
  if ((cpu_info[2] & 0x40000000) == 0x40000000) {
    rd_level = level_rdrand;
  }
#elif defined(__GNUG__)
  unsigned int info[4];
  __get_cpuid_count(0, 1, &(info[0]), &(info[1]), &(info[2]), &(info[3]));
  if ((info[2] & 0x40000000) == 0x40000000) {
    rd_level = level_rdrand;
  }
#endif
  if (rd_level != level_rdrand) {
    TSS2_RC tpm_err;
    ESYS_CONTEXT *tpm_context;
    tpm_err = Esys_Initialize(&tpm_context, nullptr, nullptr);
    if (tpm_err == TSS2_RC_SUCCESS) {
      rd_level = level_tpm;
    }
    Esys_Finalize(&tpm_context);
  }
  InitialiseEngine(rd_level);
}

CDrng::~CDrng() {
  if (rng_pointer != nullptr) {
    delete rng_pointer;
    rng_pointer = nullptr;
  }
}
void CDrng::InitialiseEngine(unsigned char level) {
  switch (level) {
  case level_rdrand:
    rng_pointer = new RDRANDRand();
    break;
  case level_tpm:
    rng_pointer = new TPMRand();
    break;
  case level_cpu:
    rng_pointer = new CPURand();
    break;
  }
}
int8_t CDrng::GetRNGQuality(CDrng *obj) { return obj->rd_level; }
int16_t CDrng::GetRandShort(CDrng *obj) {
  obj->rng_pointer->GetRandUShorts(&(obj->shrt_buf));
  return *(reinterpret_cast<int16_t *>(&(obj->shrt_buf[0])));
}
int32_t CDrng::GetRandInt(CDrng *obj) {
  obj->rng_pointer->GetRandUInts(&(obj->int_buf));
  return *(reinterpret_cast<int32_t *>(&(obj->int_buf[0])));
}
int64_t CDrng::GetRandLong(CDrng *obj) {
  obj->rng_pointer->GetRandULongs(&(obj->long_buf));
  return *(reinterpret_cast<int64_t *>(&(obj->long_buf[0])));
}
