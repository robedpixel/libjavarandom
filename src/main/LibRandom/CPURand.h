#ifndef CPURAND_H
#define CPURAND_H

#include "RandEngine.h"
#include <random>
#include <vector>
#include <algorithm>
#include <functional>

// RandEngine class using CPU
class CPURand : public RandEngine {
public:
  CPURand();
  ~CPURand();
  unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  unsigned int GetRandUInts(std::vector<uint32_t> *in);
  unsigned int GetRandULongs(std::vector<unsigned long long> *in);

private:
  using RandEngine::GetRandUShorts;
  using RandEngine::GetRandUInts;
  using RandEngine::GetRandULongs;
  std::independent_bits_engine<std::default_random_engine, 16, unsigned short>
      *cpu_engine;
  std::vector<unsigned short> buffer;
  std::vector<uint16_t> long_buffer;
};

#endif // CPURAND_H
