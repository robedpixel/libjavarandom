// Fill out your copyright notice in the Description page of Project Settings.

#include "RDRANDRand.h"

unsigned int RDRANDRand::GetRandUShorts(std::vector<uint16_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    _rdrand16_step(&(in->operator[](i)));
  }
  return 0;
}
unsigned int RDRANDRand::GetRandUInts(std::vector<uint32_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    _rdrand32_step(&(in->operator[](i)));
  }
  return 0;
}
unsigned int RDRANDRand::GetRandULongs(std::vector<unsigned long long> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    _rdrand64_step(&(in->operator[](i)));
  }
  return 0;
}

