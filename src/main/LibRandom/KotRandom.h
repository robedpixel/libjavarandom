/* Header for class KotRandom */

#ifndef _Included_KotRandom
#define _Included_KotRandom
#include "cDrng_global.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

Q_DECL_EXPORT long long p_init
  ();

/*
 * Class:     KotRandom
 * Method:    p_destroy
 * Signature: (J)I
 */
Q_DECL_EXPORT int32_t p_destroy
  (long long);

/*
 * Class:     KotRandom
 * Method:    p_getShort
 * Signature: (J)S
 */
Q_DECL_EXPORT int16_t p_getShort
  (long long);

/*
 * Class:     KotRandom
 * Method:    p_getInt
 * Signature: (J)I
 */
Q_DECL_EXPORT int32_t p_getInt
  (long long);

/*
 * Class:     KotRandom
 * Method:    p_getLong
 * Signature: (J)L
 */
Q_DECL_EXPORT long long p_getLong
  (long long);

/*
 * Class:     KotRandom
 * Method:    p_GetRNGQuality
 * Signature: (J)B
 */
Q_DECL_EXPORT int8_t p_getRNGQuality
  (long long);

#ifdef __cplusplus
}
#endif
#endif
