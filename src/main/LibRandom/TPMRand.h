// Fill out your copyright notice in the Description page of Project Settings.

#ifndef TPMRAND_H
#define TPMRAND_H

#include "RandEngine.h"
#include <cstdlib>
#include <tss2/tss2_esys.h>
#include <tss2/tss2_tpm2_types.h>

// TRNG RandEngine class using TPM2
class TPMRand : public RandEngine {
public:
  TPMRand();
  ~TPMRand();
  unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  unsigned int GetRandUInts(std::vector<uint32_t> *in);
  unsigned int GetRandULongs(std::vector<unsigned long long> *in);


private:
  using RandEngine::GetRandUShorts;
  using RandEngine::GetRandUInts;
  const unsigned int max_byte_size = 65535;
  TSS2_RC tpm_err;
  TPM2B_DIGEST *random_bytes = nullptr;
  ESYS_CONTEXT *tpm_context = nullptr;
  unsigned int num_bytes_gen;
};

#endif // TPMRAND_H
