#include "KotRandom.h" // Generated
#include "cdrng.h"
#include <iostream> // C++ standard IO header

long long p_init() {
  // find some way to store pointer to cdrng object
  CDrng *obj_pointer = new CDrng;
  return reinterpret_cast<long long>(obj_pointer);
}
int32_t p_destroy(long long pointer) {
  CDrng *obj_pointer = reinterpret_cast<CDrng *>(pointer);
  delete obj_pointer;
  return 0;
}
int16_t p_getShort(long long pointer) {
  CDrng *obj_pointer = reinterpret_cast<CDrng *>(pointer);
  int16_t output = CDrng::GetRandShort(obj_pointer);
  return output;
}

int32_t p_getInt(long long pointer) {
  CDrng *obj_pointer = reinterpret_cast<CDrng *>(pointer);
  int32_t output = CDrng::GetRandInt(obj_pointer);
  return output;
}

long long p_getLong(long long pointer) {
  CDrng *obj_pointer = reinterpret_cast<CDrng *>(pointer);
  long long output = CDrng::GetRandLong(obj_pointer);
  return output;
}

int8_t p_getRNGQuality(long long pointer) {
  CDrng *obj_pointer = reinterpret_cast<CDrng *>(pointer);
  int8_t output = CDrng::GetRNGQuality(obj_pointer);
  return output;
}
