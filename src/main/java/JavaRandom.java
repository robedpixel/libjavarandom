import java.io.File;
import java.io.InputStream;
import java.lang.foreign.*;
import java.lang.invoke.MethodHandle;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import static java.lang.foreign.ValueLayout.*;


public class JavaRandom {  // Save as HelloJNI.java
    private final Arena allocator;
    Linker linker = Linker.nativeLinker();
    SymbolLookup lib;
    MethodHandle init;
    MethodHandle destroy;
    MethodHandle nextShort;
    MethodHandle nextInt;
    MethodHandle nextLong;
    MethodHandle getRNGQuality;
    private long objPointer = 0;
    public JavaRandom(String jarPath, Arena arena){
        allocator = arena;
        try {
            System.out.println("jar location: " + jarPath);
            if (SystemUtils.IS_OS_WINDOWS){
                //do stuff if windows
                p_unpackWindowsLibraries(jarPath);
                p_loadWindowsLibraries(jarPath);
            }else if(SystemUtils.IS_OS_LINUX){
                //do stuff if linux
                p_unpackLinuxLibraries(jarPath);
                p_loadLinuxLibraries(jarPath);
            }
            p_loadMethods();
        } catch(Exception e){
            System.out.println(e);
            System.out.println("Error loading ffmrand library!");
        }
    }
    private String[] p_getlinuxdependencies(){
        return new String[] {"libtss2-sys.so.0.0.0", "libtss2-mu.so.0.0.0", "libtss2-esys.so.1.0.0", "libcrypto.so.3.1.4","libtss2-tctildr.so.0.0.0",
                "libffmrand.so"};
    }
    private String[] p_getwindowsdependencies(){
        return new String[] {"msvcp140.dll", "vcruntime140.dll", "api-ms-win-crt-heap-l1-1-0.dll",
                "api-ms-win-crt-runtime-l1-1-0.dll", "libcrypto-3-x64.dll",
                "tss2-mu.dll", "tss2-sys.dll", "tss2-tcti-tbs.dll","tss2-tctildr.dll","tss2-esys.dll", "ffmrand.dll"
        };
    }
    private void p_unpackWindowsLibraries(String path){
        String[] libArray = p_getwindowsdependencies();
        File nativeDir = new File(path + File.separator + "libjavarandom_native");
        File osDir = new File(path + File.separator + "libjavarandom_native" + File.separator + "windows");
        // create temp directories for dlls
        if (!nativeDir.exists()) {
            nativeDir.mkdir();
        }
        if (!osDir.exists()) {
            osDir.mkdir();
        }
        // extract libraries from jar
        for (String s : libArray) {
            InputStream link = (JavaRandom.class.getResourceAsStream("/native/windows/" + s));
            File dllFile = new File(path + File.separator + "libjavarandom_native" + File.separator + "windows" + File.separator + s);
            try {
                FileUtils.copyInputStreamToFile(link, dllFile);
            } catch (Exception e) {
                System.out.println("error copying dlls");
                return;
            }
        }
    }
    private void p_unpackLinuxLibraries(String path){

        String[] libArray = p_getlinuxdependencies();
        File nativeDir = new File(path + File.separator + "libjavarandom_native");
        File osDir = new File(path + File.separator + "libjavarandom_native" + File.separator + "linux");
        // create temp directories for dlls
        if (!nativeDir.exists()) {
            nativeDir.mkdir();
        }
        if (!osDir.exists()) {
            osDir.mkdir();
        }
        // extract libraries from jar

        for (String s : libArray) {
            InputStream link = (JavaRandom.class.getResourceAsStream("/native/linux/" + s));
            File dllFile = new File(path + File.separator + "libjavarandom_native" + File.separator + "linux" + File.separator + s);
            try {
                FileUtils.copyInputStreamToFile(link, dllFile);
            } catch (Exception e) {
                System.out.println("error copying dlls");
                return;
            }
        }
    }
    private void p_loadWindowsLibraries(String path){

        String[] libArray = p_getwindowsdependencies();
        for (int i=0;i<libArray.length;i++){
            System.out.println("loading: "+ path + File.separator + "libjavarandom_native"+ File.separator + "windows"+ File.separator + libArray[i]);
            System.load(path + File.separator + "libjavarandom_native"+ File.separator + "windows"+ File.separator + libArray[i]);
        }
        lib = SymbolLookup.libraryLookup("ffmrand.dll", allocator);
    }
    private void p_loadLinuxLibraries(String path){

        String[] libArray = p_getlinuxdependencies();
        for (int i=0;i<libArray.length;i++){
            System.out.println("loading: "+ path + File.separator + "libjavarandom_native"+ File.separator + "linux"+ File.separator + libArray[i]);
            System.load(path + File.separator + "libjavarandom_native"+ File.separator + "linux"+ File.separator + libArray[i]);
        }
        lib = SymbolLookup.libraryLookup("libffmrand.so", allocator);
    }
    private void p_loadMethods(){
        init = Linker.nativeLinker().downcallHandle(

                lib.find("p_init").orElseThrow(),

                FunctionDescriptor.of(JAVA_LONG));
        destroy = Linker.nativeLinker().downcallHandle(

                lib.find("p_destroy").orElseThrow(),

                FunctionDescriptor.of(JAVA_INT, JAVA_LONG));
        nextShort = Linker.nativeLinker().downcallHandle(

                lib.find("p_getShort").orElseThrow(),

                FunctionDescriptor.of(JAVA_SHORT,JAVA_LONG));
        nextInt = Linker.nativeLinker().downcallHandle(

                lib.find("p_getInt").orElseThrow(),

                FunctionDescriptor.of(JAVA_INT,JAVA_LONG));
        nextLong = Linker.nativeLinker().downcallHandle(

                lib.find("p_getLong").orElseThrow(),

                FunctionDescriptor.of(JAVA_LONG,JAVA_LONG));
        getRNGQuality = Linker.nativeLinker().downcallHandle(

                lib.find("p_getRNGQuality").orElseThrow(),

                FunctionDescriptor.of(JAVA_BYTE,JAVA_LONG));
    }
    public void init() throws Throwable {
        objPointer = (long)init.invoke();
    }
    public int destroy() throws Throwable {
        return (int)destroy.invokeWithArguments(objPointer);
    }
    public long getPointer() {return objPointer;}
    public short nextShort() throws Throwable {
        return (short)nextShort.invokeWithArguments(objPointer);
    }
    public int nextInt() throws Throwable {
        return (int)nextInt.invokeWithArguments(objPointer);
    }
    public long nextLong() throws Throwable {
        return (long)nextLong.invoke(objPointer);
    }
    public byte getRNGQuality() throws Throwable {
        return (byte)getRNGQuality.invokeWithArguments(objPointer);
    }
}
