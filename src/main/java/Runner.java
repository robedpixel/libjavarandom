import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

public class Runner {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, URISyntaxException {
        JavaRandomInstance RdGen = JavaRandomManager.getRandomInstance();
        System.out.println("random long:" + RdGen.nextLong());
        System.out.println("random int:" + RdGen.nextInt());
        System.out.println("random short:" + RdGen.nextShort());
        System.out.println("RNG Quality:" + RdGen.getRNGQuality());
    }
}
