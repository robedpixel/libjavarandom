import java.lang.reflect.InvocationTargetException;

public class JavaRandomInstance {
    private final JavaRandomManager javaRandomInstance;
    public JavaRandomInstance(JavaRandomManager managerInstance){
        this.javaRandomInstance = managerInstance;
    }
    public long nextLong() throws InvocationTargetException, IllegalAccessException {
        return javaRandomInstance.nextLong();
    }
    public int nextInt() throws InvocationTargetException, IllegalAccessException {
        return javaRandomInstance.nextInt();
    }
    public short nextShort() throws InvocationTargetException, IllegalAccessException {
        return javaRandomInstance.nextShort();
    }
    public byte getRNGQuality() throws InvocationTargetException, IllegalAccessException {
        return javaRandomInstance.getRNGQuality();
    }
}
