import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.foreign.Arena;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JavaRandomManager{
    private String jarPath;
    private CustomClassLoader cl;
    private Class<? extends JavaRandom> ca;
    private Object a;
    private Method p_init;
    private Method p_destroy;
    private Method p_getLong;
    private Method p_getInt;
    private Method p_getShort;
    private Method p_getRNGQuality;
    private Arena allocator;
    private static JavaRandomManager instance = null;
    private static final Object mutex = new Object();

    private JavaRandomManager(){
        try {
            jarPath = new File(JavaRandom.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getParent();
            allocator = Arena.ofShared();
            cl = new CustomClassLoader();
            ca = cl.findClass("JavaRandom").asSubclass(JavaRandom.class);
            a = ca.getDeclaredConstructor(String.class,Arena.class).newInstance(jarPath,allocator);
            p_init = ca.getMethod("init");
            p_destroy = ca.getMethod("destroy");
            p_getLong = ca.getMethod("nextLong");
            p_getInt = ca.getMethod("nextInt");
            p_getShort = ca.getMethod("nextShort");
            p_getRNGQuality = ca.getMethod("getRNGQuality");
            p_init.invoke(a);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    instance.close();
                } catch (InvocationTargetException | IllegalAccessException e) {
                    System.out.println(e.getMessage());
                }
            }
            ));
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    private static JavaRandomManager getInstance() {
        JavaRandomManager localRef = instance;
        if (localRef == null) {
            synchronized (mutex) {
                localRef = instance;
                if (localRef == null) {
                    instance = localRef = new JavaRandomManager();
                }
            }
        }
        return localRef;
    }
    public static JavaRandomInstance getRandomInstance(){
        return new JavaRandomInstance(getInstance());
    }

    synchronized long nextLong() throws InvocationTargetException, IllegalAccessException {
        return (long)p_getLong.invoke(a);
    }
    synchronized int nextInt() throws InvocationTargetException, IllegalAccessException {
        return (int)p_getInt.invoke(a);
    }
    synchronized short nextShort() throws InvocationTargetException, IllegalAccessException {
        return (short)p_getShort.invoke(a);
    }
    synchronized byte getRNGQuality() throws InvocationTargetException, IllegalAccessException {
        return (byte)p_getRNGQuality.invoke(a);
    }
    private void close() throws InvocationTargetException, IllegalAccessException{
        p_destroy.invoke(a);
        cl = null;
        ca = null;
        a= null;
        p_init = null;
        p_destroy = null;
        p_getLong = null;
        p_getInt = null;
        p_getShort = null;
        p_getRNGQuality = null;
        allocator.close();
        System.gc();
        System.gc();
        File nativeDir = new File(jarPath + File.separator + "libjavarandom_native");
        File osDir = new File(jarPath + File.separator + "libjavarandom_native" + File.separator + "windows");
        try {
            FileUtils.deleteDirectory(osDir);
            FileUtils.deleteDirectory(nativeDir);
        }catch(IOException e){
            System.out.println("error cleaning up libjavarandom tempfiles : failure");
        }catch(IllegalArgumentException e){
            System.out.println("error cleaning up libjavarandom tempfiles : dir does not exist");
        }
        instance =null;
    }
}
