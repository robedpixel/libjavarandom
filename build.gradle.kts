plugins {
    id("java")
    id("application")
}

group = "org.robedpixel"
version = "1.0"

application {
    mainClass = "Runner"
}
java {
    sourceCompatibility = JavaVersion.VERSION_22
    targetCompatibility = JavaVersion.VERSION_21
}

repositories {
    mavenCentral()
}
val provided = configurations.create("provided");

configurations {
    implementation {
        extendsFrom(configurations["provided"])
    }
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    provided("org.apache.commons:commons-lang3:3.12.0")
    provided("commons-io:commons-io:2.11.0")
}
tasks.withType<Jar> {
    archiveClassifier.set("SNAPSHOT")
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes["Main-Class"] = "Runner"
    }
    from("jni_lib") {
        into("native")
    }
    from(configurations["provided"].asFileTree.files.map {zipTree(it)})
}

tasks.test {
    useJUnitPlatform()
}
